/*
 * Fonctions générales pour le fonctionnement de l'interface.
 *
 */


// Affichage des messages d'erreur
errorCount = 0;
function displayError(title,content){
    errorCount ++;
    $('#error-zone').append("<div class=\"error-message\" id=\"error-"+errorCount+"\">"+
        "<div class=\"close-error-btn\"><a href='#'>✖</a></div>"+
        "<b>"+title+":</b> "+content+"</div>");
    $("#error-"+errorCount+" .close-error-btn").bind("click", {id:errorCount}, function(event){
        var errorId = event.data.id;
        $("#error-"+errorId).css("display","none");
    });
}

// Effacer la réponse de la précédente requête
function erasePreviousAnswer(){
    $("#answer-title").html("");
    $("#answer-description").html("");
}

// Affichage d'une réponse
function displayData(data){
    if(data.title){ $("#answer-title").html(data.title); }
    if(data.description){ $("#answer-description").html(data.description); }
    if(data.text){ $("#answer-text").html(data.text); }
}


function startLoading(){
    $("#loading-zone").css("display","block");
    $("#results-zone").css("margin-top","12rem")
}
function endLoading(){
    $("#loading-zone").css("display","none");
    $("#results-zone").css("margin-top","1.5rem")    
}




window.onload = function() {
    console.log('onload event')
    endLoading();
    
    $("#searchbar").on('keyup', function (e) {
        if (e.keyCode === 13) {
            ask();
        }
    });
    
    $("#searchbutton").html("Ask");
    $("#searchbutton").css("display","inline");
}
