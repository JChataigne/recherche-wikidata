/*
 * Fonctions pour charger et faire fonctionner les modèles.
 *
 */
var TFmodel;
var sentenceMaxLength = 10;   // taille des phrases (avec remplissage)
var vectorsLength = 100;      // longueur des vecteurs
var placeholderVect = new Array(vectorsLength).fill(0);


function ask(){
    console.log('ask');
    startLoading();
    
    sentence = $("#searchbar").val().toLowerCase(); // get the value
    words = sentence.split(/[,_ ']/); // split the sentence in words
    vects = gloveConvert(words);      // convert the sentence to an array of vectors
    console.log('Converted to vectors');
    
    prediction = model.predict(tf.tensor([vects]));
    predictionProp = Array.from( prediction[0].dataSync() );
    predictionObject = Array.from( prediction[1].dataSync() );
    prop = propsAdjust( argMax( predictionProp ) );
    item = findNearestGlove(predictionObject);
    
    
    if(! item){
        displayError("Item not found","couldn't find the item this search is about !");
        endLoading();
    }else{ console.log('Item found: ', item); }
    if(! prop){
        displayError("Property not found","couldn't identify the property of the object you're looking for !");
        endLoading();
    }else{ console.log('Property found: ', prop); }
    
    result = launchQuery(prop, item);
}

// Convertir une phrase en tableau de vecteurs glove
function gloveConvert(words){
    var vects = [];
    for (var i = 0; i < words.length; i++) {
        if (glove[words[i]]){
            vects.push( glove[words[i]] );
        }
    }
    // remplir avec des vecteurs de zéros pour avoir la bonne longueur
    while(vects.length < sentenceMaxLength){
        vects.push(placeholderVect);
    }
    return vects;
}

// Ajuster les numéros de propriétés de l'ordre en sortie de réseau à leur numéro dans l'API wikidata
function propsAdjust(k){
    return listePropsAdjust[k];
}

// Renvoie l'entrée GloVe la plus proche du vecteur vect donné en entrée (distance euclidienne)
function findNearestGlove(vect){
    gloveKeys = Object.keys(glove);
    min = euclideanDistance( vect, glove[gloveKeys[0]] );
    keymin = gloveKeys[0];
    for (i=1; i<gloveKeys.length; i++){
        val = euclideanDistance( vect, glove[gloveKeys[i]] );
        if ( min > val){
            min = val;
            keymin = gloveKeys[i] ;
        }
    }
    return keymin;
}
function euclideanDistance(x,y){
    v = x.map(function(val,index){ // on définit v tel que v_i = x_i - y_i
        return val - y[index];
    });
    return Math.hypot(...v); // hypot renvoie la racine de la somme des carrés des valeurs données
}

// une fonction utilitaire...
// emprunté depuis gist.github.com/engelen/fbce4476c9e68c52ff7e5c2da5c24a28
function argMax(array) {
  return array.map((x, i) => [x, i]).reduce((r, a) => (a[0] > r[0] ? a : r))[1];
}

// Charger les modèles
async function loadModels(){
    model     = await tf.loadLayersModel('../../ressources/modèles/model/model.json');
    propModel = await tf.loadLayersModel('../../ressources/modèles/modelprop/model.json');
    posModel  = await tf.loadLayersModel('../../ressources/modèles/modelpos/model.json');
}

function loadPropsAdjust(){
    listePropsAdjust = listeProps.split(",");
}



loadModels();
loadPropsAdjust();
console.log("neuralnet.js chargé");







