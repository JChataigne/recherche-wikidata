/*
 * Gestion des requêtes vers wikipédia et wikidata.
 *
 */

// Prefixes
wd = "https://www.wikidata.org/w/api.php?";
wp = "https://en.wikipedia.org/w/api.php?";
aw = "action=wbgetentities" ;
aq = "action=query" ;
s  = "&sites=enwiki" ;
ps = "&props=sitelinks|labels|aliases|descriptions|claims" ; //wikidata
pp = "&prop=pageprops" ; //wikipedia
r  = "&redirects=1" ;
c  = "&callback=?" ;
t  = "&titles=" ;
ids= "&ids=";
f  = "&format=json" ;
//l  = "&languages=en" ;


function launchQuery(propertyId,objectName){
    // This function is an endpoint to use in other parts of the code. Giving it a property id and an object name as arguments,
    // it fires a chain reaction of functions until the result of the query (or an error) is displayed on the UI.
    normalizeObject(propertyId,objectName);
}
function normalizeObject(propertyId,objectName){
    //wikipedia for normalizing and/or redirecting
    var normalizeURL = wp+aq+pp+r+t+objectName+f+c;
    $.getJSON(normalizeURL, function (data) {
        processNormalizationData(propertyId,objectName,data);
    });
}
function processNormalizationData(propertyId,objectName,data){
    newItem = objectName;
    // normalization of the name
    if(data && data.query && data.query.normalized){
        newItem = data.query.normalized[0].to;
        console.log('normalization');
    }
    // redirections to the correct name
    if(data && data.query && data.query.redirects){
        newItem = data.query.redirects[0].to;
        console.log('redirection');
    }
    // Check that data was found and send query
    if(typeof data !== 'undefined'){
        queryProperty(propertyId,newItem);
    }else{
        displayError('Item not found','no data was returned while querying for an object named '+objectName);
        endLoading();
    }    
}

function queryProperty(propertyId,itemName){
    // Return the object id of the property of the object itemName
    queryURL = wd+aw+s+ps+t+itemName+f+c;
    return $.getJSON(queryURL, function(data) {processQueryProperty(propertyId,itemName,data);} );
}
function processQueryProperty(propertyId,itemName,data){
    // Check that data is defined correctly and send the next query
    if(data && data.entities && data.entities[ Object.keys(data.entities)[0]] && data.entities[ Object.keys(data.entities)[0]].claims && data.entities[ Object.keys(data.entities)[0]].claims[propertyId]  ){
        answerType = data.entities[ Object.keys(data.entities)[0] ].claims[propertyId][0].mainsnak.datatype;
        answerObjectId = data.entities[ Object.keys(data.entities)[0] ].claims[propertyId][0].mainsnak.datavalue.value;
        processData(answerObjectId, answerType);
    }else{
        displayError('Answer not found','something went wrong while querying the property '+propertyId+' of the object "'+itemName+'".');
        endLoading();
    }
}
function processData(data,datatype){
    switch(datatype){
        case "wikibase-item":
            queryObjectCharacteristics(data.id); break;
        case "quantity":
            processQuantity(data); break;
        case "time":
            processTime(data); break;
        case "monolingualtext":
            displayText(data.text); break;
        case "string":
            displayText(data); break;
        default:
            displayError("Unknown data type","an unknown or void data type was encountered: "+datatype);
            endLoading();
    }
}
function processQuantity(data){
    //objet contenant amount, lowerbound, upperbound et unit (lien vers un objet)
    if (data.unit){
        var unitId = data.unit.substr(data.unit.lastIndexOf('/') + 1 );
        var unitURL = wd+aw+s+ps+ids+unitId+f+c;
        $.getJSON( unitURL, function (unitData) { processUnitData(data, unitData); } );
    }else{
        displayData({'title':data.amount}) ;
    }
}
function processUnitData(data, unitData){
    if (unitData){
        var unitText = unitData.entities[ Object.keys(unitData.entities)[0] ].labels.en.value;
        titleToDisplay = Number(data.amount).toString() + " " + unitText ;
        if (Number(data.amount) != 1) { titleToDisplay += "s" }
        displayData({'title': titleToDisplay });
    }
}
function processTime(data){
    //objet contenant time,timezone,before,after,precision (en nb de caractères de time) et calendarmodel (lien vers un objet)
    time = data.time.substring( Number(data.precision) );
    if (time.substring(0,1) == "+") { time = time.substring(1) ; }
    timeToDisplay = {"title":time};
    if ( Number(data.precision) > 11 ){ timeToDisplay.description = "GMT "+data.timezone ; }
    displayData(timeToDisplay);
}
function queryObjectCharacteristics(answerObjectId){
    console.log("answerObjectId",answerObjectId)
    // Query the answer's name and characteristics
    if(typeof answerObjectId !== 'undefined'){
        var answerURL = wd+aw+s+ps+ids+answerObjectId+f+c;
        $.getJSON(answerURL, function (data) { processQueryObject(data); });
    }else{
        displayError('Item not found','could not find a wikidata object with ID \"'+answerObjectId+'\"');
        endLoading();
    }
}
function processQueryObject(data) {
    // display the data on the UI
    if(data){
        console.log(data);
        var title = data.entities[ Object.keys(data.entities)[0] ].labels.en.value;
        var description = data.entities[ Object.keys(data.entities)[0] ].descriptions.en.value;
        displayData({'title':title,'description':description});
        endLoading();
    }
}
function displayText(text){
    displayData({'texte':text});
}






