# Recherche wikidata

## Démarrage rapide

Le projet est divisé entre le module Python qui sert à générer et entraîner
les réseaux de traitement du langage, et l'interface web qui sert à les utiliser.

Pour tester le résultat final (interface seule, sans besoin de réentraîner
les réseaux):
- clonez ce dépôt
- ouvrez un terminal dans le dossier du projet
- exécutez `python -m SimpleHTTPServer` (Python 2)
    ou `python -m http.server` (Python 3)
- ouvrez votre navigateur sur http://localhost:8000/interface_src/front-end.html


## Prochaines étapes du développement

- sortie sous forme de vecteur pour le prédicat/l'objet de la recherche. Pour l'instant on sort la position du mot dans la phrase, il faudrait plutôt sortir un vecteur du même format que GloVe. Ainsi on pourra prendre en sortie le vecteur GloVe le plus proche.
- Fusionner les deux réseaux (celui pour la proposition et celui pour le prédicat)
- Retourner les sortie les plus probables (au lieu d'une seule), afin de pouvoir en chercher plusieurs dans wikidata. Éventuellement entraîner le réseau avec une fonction d'évaluation top-5? 
- ajouter un fichier de paires entrées-sorties pour n'avoir pas que des phrases générées automatiquement


## Détails sur le fonctionnement

Les entrées et sorties doivent être les suivantes:  
* Entrée : *phrase en langage naturel*
* Sortie : *requête wikidata* (`propriété` , `objet`)

La requête consiste à rechercher la valeur de la propriété `propriété`
du prédicat `objet`, avec `objet` une chaîne de caractères et `propriété`
un identifiant wikidata de propriété
(voir [Introduction Wikidata](https://www.wikidata.org/wiki/Wikidata:Introduction)
pour plus de détails).  
Par exemple:  
"Quelle est la capitale de la France" -> (P36 , "France")

Il y a pour l'instant (mais cela devrait changer bientôt) deux réseaux distincts:
un pour trouver la propriété, et l'autre pour trouver l'objet.
Notez que pour l'instant (mais cela aussi devrait changer rapidement),
on n'utilise pas l'objet lui-même mais sa position dans la phrase
pour entraîner le réseau.

Comme il n'existe pas de base de données contenant de telles associations
d'entrées-sorties, on utilise ici un générateur de phrases pour tenter
de simuler une telle base de données.
Lorsqu'une phrase est générée, elle est convertie en vecteurs grâce à 
[GloVe](nlp.stanford.edu/projects/glove/) avant d'être donnée en entrée au neurone.
L'objet ou la propriété générés en même temps sont eux donnés au réseau
en tant que sortie attendue.
Une fois entraîné, le réseau est enregistré dans un format lisible par tensorflowjs.

Lors de l'utilisation de l'interface graphique,
on utilise la version précédemment enregistrée du réseau pour traiter
les requêtes tapées dans le navigateur.
Les vecteurs de GloVe doivent aussi être chargés, ce qui prend un certain temps
(n'ayant pas de serveur, tout s'exécute côté client).


## Organisation des fichiers

| Fichier                      | Définition  |
|------------------------------|---|
| interface_src                | Sources pour l'interface graphique |
| Python_src                   | Sources pour le code Python (génération des modèles) |
| ressources                   | Regroupe les ressources utilisées.  |
| ressources/modèles           | Sert de pont entre Python et l'interface: les modèles générés par Python y sont stockés, et l'interface va les y chercher au moment de traiter les requêtes.  |
| ressources/glove             | Contient les correspondances entre mots et vecteurs (utilisés par l'interface et les scripts de génération des modèles)  |
| ressources/liste-propriétés  |  Voir ci-dessous |
| ressources/listes-mots       | Contient les listes de mots utilisées pour la génération des phrases lors de l'entraînement des modèles.  |



**liste-propriétés** :
Contient les listes des propriétés wikidata à utiliser
(utilisées par l'interface et les scripts de génération des modèles),
ainsi qu'une interface (fichier html) pour établir une liste des propriétés
qu'on veut sélectionner.
Il y a une liste contenant uniquement les identifiants de propriétés
(P17, P19, etc.) et pour chaque langue (pour l'instant uniquement anglais)
une autre contenant les identifiants ainsi que les alias correspondants.
De plus il existe des versions temporaires de ces listes ne contenant quúne partie
des propriétés, et une version différente pour un usage par l'interface.



| Fichier | Convention de nommage |
| ------- | --------------------- |
|                 | Interface : `props-for-web-use.js` |
| liste-propriété | Scripts (sans alias) : `props-to-save.json` |
|                 | Scripts (avec alias dans la langue *lang*) : `props-to-save-<lang>.json` |
|                 | (Temporaire) Ajouter avant l'extension `-temp<no>`, où *no* est le numéro d'id de la plus haute propriété parcourue lors de la génération de la liste. |
| listes-mot      | liste_`code langue`_`nombre de mots` |
