#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 14:39:04 2020

@author: jchataigne
"""
# when using interactive Python shell, import importlib and do "importlib.import_module('NeuralNetClass')"
from random import choice
from random import randrange

import numpy as np
import tensorflowjs as tfjs
# Use Gensim to embed words
from gensim.models import KeyedVectors
from keras.callbacks import History
from keras.engine import InputLayer
from keras.layers import Dense, LSTM, Input, Flatten
from keras.models import Sequential, Model
from nltk.tokenize import RegexpTokenizer
import matplotlib.pyplot as plt


class NeuralNet:

    def __init__(self, fileglove='../ressources/glove/glove.6B.100d.txt.word2vec', 
                 lang="en", pSet="temp100", sentenceMaxLength=10, batch_size=5,
                 epochs=100, steps_per_epoch=1000, valid_steps=100):
        self.fileglove = fileglove
        self.sentenceMaxLength = sentenceMaxLength
        self.tokenizer = RegexpTokenizer(r'\w+')

        print("Loading GloVe word embeddings...")
        # Loads the Stanford GloVe model. Code was adapted from
        # https://machinelearningmastery.com/develop-word-embeddings-python-gensim /
        self.glove = KeyedVectors.load_word2vec_format(self.fileglove, binary=False)
        self.propIDs, self.propsAliases = [], []

        print("Loading properties list...")
        #TODO remplacer par tjrs les mêmes props
        self.load_props('props-to-save-' + lang + '-' + pSet + '.json')
        self.nProps = len(self.propIDs)

        print("Loading objects file...")
        #self.objectsFilePath = "" # je garde ça un moment pour voir si ça plante la prochaine fois
        if lang == 'en':
            self.objectsFilePath = '../ressources/listes-mots/liste_en_58110.txt'
        elif lang == 'fr':
            self.objectsFilePath = '../ressources/listes-mots/liste_fr_22740.txt'
        self.objectslist = []
        with open(self.objectsFilePath) as file_in:
            for line in file_in:
                self.objectslist.append(line)

        print("Initializing learning parameters...")
        self.vector_size = len(self.glove["a"]) # taille vecteur
        self.batch_size = batch_size
        self.time_steps = sentenceMaxLength
        self.epochs = epochs
        self.steps_per_epoch = steps_per_epoch
        self.valid_steps = valid_steps
        self.train_generator = self.dataGenerator()
        self.valid_generator = self.dataGenerator()
        self.model = ""
        print("Finished setting up object !")

    def load_props(self, fileprops):
        """ 
            Reads the properties stocked in <fileprops> with their aliases and
            sets them as the object variables propIDs and propsAliases.
            Format of lines in the file: Pxx,alias1,alias2,alias3
        """
        f = open('../ressources/liste-proprietes/' + fileprops, "r")
        for line in f:
            prop = line.rstrip('\n').split(',')
            aliases = []
            for alias in prop[1:]:  # prop[0] est l'id de propriété
                try:
                    self.glove[alias]
                    aliases.append(alias)
                except:
                    pass # il faut vérifier que les alias ont des vecteurs définis
            if len(aliases) > 0:
                self.propIDs.append(prop[0])
                self.propsAliases.append(aliases)
        f.close()

    def set_param_model(self, **kwargs):
        """
            Permet de modifier les paramètres du modèle de ML sans tout relancer
        """
        self.epochs = kwargs["epochs"]
        self.steps_per_epoch = kwargs["steps_per_epoch"]
        self.valid_steps = kwargs["valid_steps"]

    def generate_train(self):
        print("Generating and training model...")
        self.generate_model()
        self.compute_model()
        print("Saving model...")
        #TODO : réécrire une fonction pour enregistrer les modèles
        # modelsavefile = 'modèles/modelprop' if propOrObject else 'modèles/modelpos'
        # tfjs.converters.save_keras_model(model, modelsavefile)
        # print("Model saved !")

    def dataGenerator(self):
        """Put makeBatch function in a generator."""
        while True:
            X_batch, y_batch = self.makeBatch()
            yield X_batch, y_batch

    def makeBatch(self):
        """
            Generate a batch of batch_size vectorized sentences with corresponding
            output. 
        """
        sentences, y_batch = [], { "head_prop":[], "head_object":[] }
        for _ in range(self.batch_size):
            x, y = self.genSentence()
            assert len(x) == self.time_steps, str(len(x)) + " != " + str(self.time_steps)
            assert len(x[0]) == self.vector_size, str(len(x[0])) + " != " + str(self.vector_size)
            sentences.append(x)
            y_batch['head_prop'].append(y['head_prop'])
            y_batch['head_object'].append(y['head_object'])
        sentences = np.array(sentences)
        y_batch['head_prop'] = np.array(y_batch['head_prop'])
        y_batch['head_object'] = np.array(y_batch['head_object'])
        return sentences, y_batch

    def genSentence(self):
        """
            Generate Data: generates a sentence, transforms it into a sequence of
            vectors and returns it along with the corresponding output.
        """
        propIndex = randrange(self.nProps) # choix propriété aléatoire
        alias = choice(self.propsAliases[propIndex]) # choix alias aléatoire
        objectName = self.getObjectName() # nom d'objet aléatoire
        # génération phrase
        if randrange(2) == 0:
            sentence = 'what is the ' + alias + ' of ' + objectName
        else:
            sentence = 'what is ' + objectName + ' s ' + alias
        Vsentence = self.vectorize(sentence)
        VpropIndex = self.oneHotPropIndex(propIndex)
        Vobject = self.glove[objectName]
        return Vsentence, { "head_prop":VpropIndex, "head_object":Vobject }

    def getObjectName(self):
        """
            Returns a random object name
        """
        restart = True
        while restart:
            restart = False
            word = choice(self.objectslist)[:-1]
            try:
                self.glove[word] # on vérifie que word est bien dans glove
            except:
                restart = True
        return word

    #TODO supprimable
    def findObjectPos(self, sentence, objectName):
        """Return objectName's starting and ending positions in the sentence"""
        splitSentence, splitObj = sentence.split(), objectName.split()
        for k in range(len(splitSentence)):
            if splitSentence[k] == splitObj[0]:
                objectPos = k
                objectEnd = k + len(splitObj) - 1
                break
        return objectPos, objectEnd

    def vectorize(self,sentence):
        """Transforms a sentence into a sequence of word vectors"""
        sentenceV = []
        for word in self.tokenizer.tokenize(sentence):
            sentenceV.append(self.glove[word])
        while len(sentenceV) < self.time_steps:
            sentenceV.append([0. for _ in range(self.vector_size)])
        return sentenceV

    def oneHotPropIndex(self,propIndex):
        """transform propIndex into a one-hot vector"""
        vect = [0. for _ in range(len(self.propIDs))]
        vect[propIndex] = 1.
        return vect

    #TODO: supprimable
    def oneHotObjectPos(self, objectPos, objectEnd):
        """
            Transform object positions into a one-hot vector.
            ATTENTION: if the object name length is more than one word there is
            another 1 value in the vector, at the ending position.
        """
        vect = [0. for _ in range(self.sentenceMaxLength)]
        vect[objectPos] = 1.
        vect[objectEnd] = 1.
        return vect

    def generate_model(self):
        """
            Génère un modèle par defaut
        """
        #self.model = Sequential()
        #self.model.add(LSTM(128, dropout=0.2, input_shape=(self.time_steps, self.vector_size)))
        #self.model.add(Dense(self.output_dim, activation='sigmoid'))
        input_layer = Input(shape=(self.time_steps, self.vector_size))
        
        common_branch = Dense(128, activation='relu')(input_layer)
        common_branch = Dense(256, activation='relu')(common_branch)
        common_branch = Flatten()(common_branch)
        common_branch = Dense(1024, activation='relu')(common_branch)
        
        branch_prop = Dense(self.nProps, activation='softmax', name='head_prop')(common_branch)
        
        branch_object = Dense(512, activation='relu')(common_branch)
        branch_object = Dense(self.vector_size, activation='relu', name='head_object')(branch_object)
        
        self.model = Model(inputs=input_layer,outputs=[branch_prop,branch_object])
        
        
        losses = {"head_prop":"categorical_crossentropy", "head_object":"mean_squared_error"}
        self.model.compile(optimizer="adam",
                           loss=losses,
                           metrics=['accuracy'])

    def set_model(self, model):
        """
            Mettre en place son propre modèle
        """
        self.model = model

    def compute_model(self):
        """
            Lancer l'entrainement du modèle
        """

        self.model.fit(x=self.train_generator,
                                 steps_per_epoch=self.steps_per_epoch,
                                 epochs=self.epochs,
                                 validation_data=self.valid_generator,
                                 validation_steps=self.valid_steps,
                                 verbose=1
                                 )


test = NeuralNet()
test.generate_model()
test.compute_model()

try:
    test.model.save('../ressources/modèles/model/model.h5')
except:
    pass

tfjs.converters.save_keras_model(test.model, '../ressources/modèles/model/')
