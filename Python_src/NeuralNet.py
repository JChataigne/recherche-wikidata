#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 14:39:04 2020

@author: jchataigne
"""
from random import choice
from random import randrange

import numpy as np
# import tensorflowjs as tfjs
# Use Gensim to embed words
from gensim.models import KeyedVectors
from keras.callbacks import History
from keras.engine import InputLayer
from keras.layers import Dense, LSTM
from keras.models import Sequential
from nltk.tokenize import RegexpTokenizer
import matplotlib.pyplot as plt


def loadGloVeModel():
    """
        Loads the Stanford GloVe model.
        From https://machinelearningmastery.com/develop-word-embeddings-python-gensim/
    """
    filename = '../ressources/glove/glove.6B.100d.txt.word2vec'
    global glove  # définir une variable globale
    glove = KeyedVectors.load_word2vec_format(filename, binary=False)


def loadProps(filename):
    """ Reads the properties stocked in <filename> with their aliases and sets them as global variables. """
    # Specify filename
    f = open('../ressources/liste-proprietes/' + filename, "r")
    global propIDs, propsAliases, nProps  # définition variables globales
    propIDs, propsAliases = [], []
    for line in f:
        prop = line.rstrip('\n').split(',')
        aliases = []
        for alias in prop[1:]:  # si prop = 'P12', prop[1:] donne '12'
            try:
                glove[alias]
                aliases.append(alias)
            except:
                pass
        if len(aliases) > 0:
            propIDs.append(prop[0])
            propsAliases.append(aliases)
    f.close()
    nProps = len(propIDs)


def findObjectPos(sentence, objectName):
    """Return objectName's starting and ending positions in the sentence"""
    splitSentence, splitObj = sentence.split(), objectName.split()
    for k in range(len(splitSentence)):
        if splitSentence[k] == splitObj[0]:
            objectPos = k
            objectEnd = k + len(splitObj) - 1
            break
    return objectPos, objectEnd


def vectorize(sentence):
    """Transforms a sentence into a sequence of word vectors"""
    sentenceV = []
    for word in tokenizer.tokenize(sentence):
        sentenceV.append(glove[word])
    while len(sentenceV) < time_steps:
        sentenceV.append([0. for _ in range(input_dim)])
    return sentenceV


def oneHotPropIndex(propIndex):
    """transform propIndex into a one-hot vector"""
    vect = [0. for _ in range(len(propIDs))]
    vect[propIndex] = 1.
    return vect


def oneHotObjectPos(objectPos, objectEnd):
    """
        Transform object positions into a one-hot vector.
        ATTENTION: if the object name length is more than one word there is
        another 1 value in the vector, at the ending position.
    """
    vect = [0. for _ in range(sentenceMaxLength)]
    vect[objectPos] = 1.
    vect[objectEnd] = 1.
    return vect


def getObjectName():
    """returns a random object name"""
    restart = True
    while restart:
        restart = False
        word = choice(objectslist)[:-1]
        try:
            glove[word]
        except:
            restart = True
    return word


def genSentence():
    """
        Generate Data: generates a sentence, transforms it into a sequence of
        vectors and returns it along with the corresponding property's index, or
        object position depending on the value of global variable propOrObject.
    """
    propIndex = randrange(nProps)
    try:
        alias = choice(propsAliases[propIndex])
    except:
        pass
    objectName = getObjectName()
    if randrange(2) == 0:
        sentence = 'what is the ' + alias + ' of ' + objectName
    else:
        sentence = 'what is ' + objectName + ' s ' + alias
    objectPos, objectEnd = findObjectPos(sentence, objectName)
    Vsentence = vectorize(sentence)
    VpropIndex = oneHotPropIndex(propIndex)
    VobjectPos = oneHotObjectPos(objectPos, objectEnd)
    if propOrObject:
        return Vsentence, VpropIndex
    else:
        return Vsentence, VobjectPos


def makeBatch(batch_size):
    """
        Generate a batch of batch_size vectorized sentences with corresponding
        property ID or object. The global parameter propOrObject specifies whether y is
        the property ID (True) or the object position (False).
    """
    sentences, y_batch = [], []
    for _ in range(batch_size):
        x, y = genSentence()
        assert len(x) == time_steps, str(len(x)) + " != " + str(time_steps)
        assert len(x[0]) == input_dim, str(len(x[0])) + " != " + str(input_dim)
        assert len(y) == output_dim, str(len(y)) + " != " + str(output_dim)
        sentences.append(x)
        y_batch.append(y)
    return sentences, y_batch


# copied code from stackoverflow.com/questions/44569938
def dataGenerator(batch_size):
    """Put makeBatch function in a generator."""
    while True:
        X_batch, y_batch = makeBatch(batch_size)
        yield np.array(X_batch), np.array(y_batch)


def modelProps():
    """
        Keras model to process data. This is the model to recognize properties.
    """
    model = Sequential()
    model.add(LSTM(128, dropout=0.2, input_shape=(time_steps, input_dim)))
    model.add(Dense(output_dim, activation='sigmoid'))

    model.compile(optimizer='adam',
                  loss='mse',
                  metrics=['accuracy'])

    model.fit_generator(generator=train_generator,
                        steps_per_epoch=steps_per_epoch,
                        epochs=epochs,
                        validation_data=valid_generator,
                        validation_steps=valid_steps,
                        verbose=0
                        )
    return model


def modelPos():
    """
        Keras model to process data. This is the model to recognize the object
        position.
    """
    model = Sequential()
    model.add(LSTM(128, dropout=0.2, input_shape=(time_steps, input_dim)))
    model.add(Dense(output_dim, activation='sigmoid'))

    model.compile(optimizer='adam',
                  loss='mse',
                  metrics=['accuracy'])

    model.fit_generator(generator=train_generator,
                        steps_per_epoch=steps_per_epoch,
                        epochs=epochs,
                        validation_data=valid_generator,
                        validation_steps=valid_steps,
                        verbose=0
                        )
    return model


def main():
    """Load utilities and launch learning"""
    global tokenizer
    tokenizer = RegexpTokenizer(r'\w+')

    print("Loading GloVe word embeddings...")
    loadGloVeModel()

    print("Loading properties list...")
    propsfile = '../ressources/props-to-save-' + lang + '-' + pSet + '.json'
    loadProps(propsfile)

    print("Loading objects file...")
    if lang == 'en':
        objectsFilePath = '../ressources/listes-mots/liste_en_58110.txt'
    elif lang == 'fr':
        objectsFilePath = '../ressources/listes-mots/liste_fr_22740.txt'
    global objectslist
    objectslist = []
    with open(objectsFilePath) as file_in:
        for line in file_in:
            objectslist.append(line)

    print("Initializing learning parameters...")
    global output_dim, input_dim, batch_size, time_steps, epochs, steps_per_epoch, valid_steps
    output_dim = nProps if propOrObject else sentenceMaxLength
    input_dim = len(glove["a"])
    batch_size = 5
    time_steps = sentenceMaxLength
    epochs = 100

    steps_per_epoch = 100
    valid_steps = 100
    global train_generator, valid_generator
    train_generator = dataGenerator(batch_size)
    valid_generator = dataGenerator(batch_size)

    print("Generating and training model...")
    if propOrObject:
        model = modelProps()
    else:
        model = modelPos()
    print("Saving model...")
    # modelsavefile = '../ressources/modèles/modelprop' if propOrObject else 'modèles/modelpos'
    # tfjs.converters.save_keras_model(model, modelsavefile)
    print("Model saved !")


print(" -- Start -- ")
# specifies whether the output shall be the property ID (True) or the object position (False).
propOrObject = False
lang = 'en'
pSet = 'temp100'  # set of properties
sentenceMaxLength = 10

main()

print("Fini")

######## NETWORK TESTING ZONE ########


model = Sequential()
model.add(InputLayer(input_shape=(time_steps, input_dim)))
# model.add(SimpleRNN(2048, return_sequences=False))
# model.add(Conv1D(filters=64, kernel_size=5))
# model.add(Dense(2048, activation='relu'))
model.add(LSTM(128, dropout=0.2))
model.add(Dense(output_dim, activation='sigmoid'))

model.compile(optimizer='adam',
              loss='mse',
              metrics=['accuracy'])

history = History()
model.fit_generator(generator=train_generator,
                    steps_per_epoch=steps_per_epoch,
                    epochs=epochs,
                    validation_data=valid_generator,
                    validation_steps=valid_steps,
                    callbacks=[history],
                    verbose=0
                    )

# Show accuracy and loss curves
plt.close('all')  # close remaining windows if there are some

train_accuracy = history.history['accuracy']
train_loss = history.history['loss']
validation_accuracy = history.history['val_accuracy']
validation_loss = history.history['val_loss']

plt.figure(0)
plt.plot(train_accuracy, label='training accuracy')
plt.plot(validation_accuracy, label='validation accuracy')
plt.legend()
plt.title('Accuracy')
plt.show()
plt.figure(1)
plt.plot(train_loss, label='training loss')
plt.plot(validation_loss, label='validation loss')
plt.legend()
plt.title('Loss')
plt.show()
