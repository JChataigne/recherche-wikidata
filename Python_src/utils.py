#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 11:12:09 2020

@author: jchataigne
"""

from gensim.scripts.glove2word2vec import glove2word2vec


def createGloveFile(codelength):
    """
        Copies the GloVe file and converts it to the word2vec format, following
        the method from
        https://machinelearningmastery.com/develop-word-embeddings-python-gensim/
        The argument codelength is the length of the word vectors (50, 100, 200 or 300).
        Fonction à adapter selon l'endroit où vous avez téléchargé les données de GloVe !
        exemple
    """
    glove2word2vec('../../package_utils/glove.6B/glove.6B.' + str(codelength) + 'd.txt',
                   '../ressources/glove/glove.6B.' + str(codelength) + 'd.txt.word2vec')


# createGloveFile(100)

def JSONifyGlove(codelength):
    """
        Converts the GloVe file to JSON format.
        The argument codelength is the length of the word vectors (50, 100, 200 or 300).
        Fonction à adapter selon l'endroit où vous avez téléchargé les données de GloVe !
    """
    with open('../../package_utils/glove.6B/glove.6B.100d.txt') as infile:
        # with open('./jsonifytestglove.txt') as infile:
        firstDataLine = True
        jsonObj = "var glove={"
        for line in infile:
            if firstDataLine:
                firstDataLine = False
                lineToWrite = ""
            else:
                lineToWrite = ","
            values = line.split(' ')
            if values[0] == "\"":
                lineToWrite += "\"\\\"\":[" + values[1]
            elif values[0] == "\\":
                lineToWrite += "\"\\\\\":[" + values[1]
            else:
                lineToWrite += "\"" + values[0] + "\":[" + values[1]
            for val in values[2:]:
                lineToWrite += "," + val
            lineToWrite += "]"
            jsonObj += lineToWrite
        jsonObj += "}"
        # save to file
        outputFile = open("../ressources/glove" + str(codelength) + ".js", "w")
        outputFile.write(jsonObj)
        outputFile.close()


#JSONifyGlove(100)
